"""OpenWeatherTest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url

from rest_framework import routers

from backend.views.item import new_item, delete_item, list_items
from backend.views.registration import RegistrationView
from backend.views.login import LoginView
from backend.views.transfer import send, receive

router = routers.DefaultRouter()

urlpatterns = [
    # path('admin/', admin.site.urls),
    # url(r'^api/v1/', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls')),
    # url(r'^api-token-auth/', views.obtain_auth_token),

    url(r'^api/v1/registration/', RegistrationView, name="registration"),
    url(r'^api/v1/login/', LoginView, name="login"),
    url(r'^api/v1/items/new/', new_item, name="new_item"),
    url(r'^api/v1/items/(?P<item_id>\d+)/$', delete_item, name="delete_item"),
    url(r'^api/v1/items/', list_items, name="list_items"),
    url(r'^api/v1/send/', send, name="send"),
    url(r'^api/v1/get/(?P<transfer_id>\d+)/$', receive, name="receive"),
]
