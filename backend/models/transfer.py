from django.contrib.auth.models import User
from django.db import models

from backend.models import Item


class Transfer(models.Model):

    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} -> {1}".format(self.item, self.receiver)
