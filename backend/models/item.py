from django.contrib.auth.models import User
from django.db import models


class Item(models.Model):

    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{0}".format(self.name)
