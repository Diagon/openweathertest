from rest_framework.decorators import authentication_classes, api_view
from rest_framework.response import Response
from rest_framework import status


from backend.auth import ExpiringTokenAuthentication
from backend.models.item import Item


@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
def new_item(request):
    item_name = request.POST.get("name")
    user = request.user

    if item_name is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    Item.objects.create(name=item_name, owner=user)

    return Response(f"Item {item_name} created")


@api_view(['DELETE'])
@authentication_classes([ExpiringTokenAuthentication])
def delete_item(request, item_id):
    user = request.user

    try:
        item = Item.objects.get(pk=item_id)
        if item.owner == user:
            item.delete()
            return Response("Item Deleted")
        else:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    except Item.DoesNotExist:
        return Response("Item does not exist.")


@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
def list_items(request):
    user = request.user

    items = Item.objects.filter(owner=user)

    return Response({
        item.pk: item.name for item in items
    })
