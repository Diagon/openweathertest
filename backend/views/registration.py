from django.contrib.auth.models import User
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from rest_framework import status


@api_view(["POST"])
@authentication_classes([])
def RegistrationView(request):

    login = request.POST.get("login")
    password = request.POST.get("password")

    if login is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    if password is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    if User.objects.filter(username=login).exists():
        return Response("User {0} already exists.".format(login))
    else:
        User.objects.create_user(username=login, password=password)
        return Response("User {0} is successfully registered.".format(login))
