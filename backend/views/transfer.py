from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.decorators import authentication_classes, api_view
from rest_framework.response import Response
from rest_framework import status

from backend.auth import ExpiringTokenAuthentication
from backend.models.item import Item
from backend.models.transfer import Transfer


@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
def send(request):

    user = request.user
    item_id = request.POST.get("item_id")
    receiver_login = request.POST.get("login")

    if item_id is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    if receiver_login is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    try:
        item = Item.objects.get(pk=item_id)
    except Item.DoesNotExist:
        return Response("Item does not exist.")

    try:
        receiver = User.objects.get(username=receiver_login)
    except User.DoesNotExist:
        return Response("User does not exist.")

    if item.owner == user:
        transfer = Transfer.objects.create(item=item, receiver=receiver)
        url = reverse('receive', kwargs={'transfer_id': transfer.pk})
        absolute_url = request.build_absolute_uri(url)
        return Response(absolute_url)
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['GET'])
@authentication_classes([ExpiringTokenAuthentication])
def receive(request, transfer_id):
    user = request.user

    try:
        transfer = Transfer.objects.get(pk=transfer_id)
    except Transfer.DoesNotExist:
        return Response("Item does not exist.")

    if transfer.receiver == user:
        transfer.item.owner = request.user
        transfer.item.save()

        Transfer.objects.filter(item=transfer.item).delete()
        return Response("Item successfully transferred")
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
