from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User


@api_view(["POST"])
@authentication_classes([])
def LoginView(request):

    login = request.POST.get("login")
    password = request.POST.get("password")

    if login is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    if password is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    try:
        user = User.objects.get(username=login)
        if check_password(password=password, encoded=user.password):
            token, created = Token.objects.get_or_create(user=user)
            return Response(token.key)

        else:
            return Response("Wrong password.".format(login))

    except User.DoesNotExist:
        return Response("User {0} does not exist.".format(login))
