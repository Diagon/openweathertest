
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from backend.models import Item
from backend.models.transfer import Transfer


class APITests(APITestCase):
    api_version = 'v1'
    base_url = "/api/{0}/".format(api_version)

    @classmethod
    def setUpTestData(cls):
        cls.u = User.objects.create_user(username='testuser', password='secret')

    def force_auth(self):
        self.client = APIClient()
        self.client.force_authenticate(user=self.u)

    def test_registration(self):
        url = self.base_url + "registration/"

        data = {
            "login": "alice",
            "password": "secret",
        }

        self.assertEqual(User.objects.count(), 1)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), 2)
        self.assertTrue("alice" in response.data)

        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login(self):
        # Wrong password
        url = self.base_url + "login/"
        data = {
            "login": "testuser",
            "password": "wrongsecret",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "Wrong password.")

        # User does not exist
        data = {
            "login": "wrong",
            "password": "secret",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "User wrong does not exist.")

        # Empty request
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Success
        data = {
            "login": "testuser",
            "password": "secret",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_new_item(self):
        url = self.base_url + "items/new/"
        data = {
            "name": "something",
        }

        # Not authorized
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.force_auth()

        # Empty request
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Success
        data = {
            "name": "something",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_item(self):
        user2 = User.objects.create_user(username='testuser2')
        item1 = Item.objects.create(name="something", owner=self.u)
        item2 = Item.objects.create(name="something", owner=user2)
        url = self.base_url + f"items/{item2.pk}/"

        # Unauthorized
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.force_auth()

        # Trying to delete other user's item
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        url = self.base_url + f"items/{item1.pk}/"

        # Success
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_items(self):
        url = self.base_url + f"items/"

        # Unauthorized
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.force_auth()

        # Empty list
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {})

        item1 = Item.objects.create(name="something", owner=self.u)
        item2 = Item.objects.create(name="something else", owner=self.u)

        user2 = User.objects.create_user(username='testuser2')
        item3 = Item.objects.create(name="somebody's", owner=user2)

        # Filtered list
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            item1.pk: item1.name,
            item2.pk: item2.name,
        })

    def test_send(self):
        url = self.base_url + f"send/"

        # Unauthorized
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.force_auth()

        item = Item.objects.create(name="something", owner=self.u)
        user2 = User.objects.create_user(username='testuser2')

        # Emtpy request
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Success
        data = {
            "item_id": f"{item.pk}",
            "login": "testuser2",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_receive(self):

        user1 = User.objects.create_user(username='testuser2')  # owner
        user2 = User.objects.create_user(username='testuser3')  # malicious
        # self.u - receiver

        item = Item.objects.create(name="something", owner=user1)
        transfer = Transfer.objects.create(item=item, receiver=self.u)

        url = self.base_url + f"get/{transfer.pk}/"

        # Unauthorized
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Malicious
        self.client = APIClient()
        self.client.force_authenticate(user=user2)

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        item.refresh_from_db()
        self.assertEqual(item.owner, user1)

        # Success
        self.client.force_authenticate(user=self.u)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        item.refresh_from_db()
        self.assertEqual(item.owner, self.u)
